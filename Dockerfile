# Start from a Debian image with the latest version of Go installed
# and a workspace (GOPATH) configured at /go.
FROM golang

# Build the outyet command inside the container.
# (You may fetch or manage dependencies here,
# either manually or with a tool like "godep".)
RUN go get bitbucket.org/yeyus/go-seared
RUN go install bitbucket.org/yeyus/go-seared

# Run the outyet command by default when the container starts.
ENTRYPOINT ["/go/bin/go-seared"]
