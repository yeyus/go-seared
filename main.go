package main

import (
	"flag"
	"io"
	"log"
	"os"
	"strconv"
	"time"

	"bitbucket.org/yeyus/jnap"
	MQTT "github.com/eclipse/paho.mqtt.golang"
)

var (
	// Trace logger
	Trace *log.Logger
	// Info logger
	Info *log.Logger
	// Warning logger
	Warning *log.Logger
	// Error logger
	Error *log.Logger
)

func main() {
	var router, broker, user, pass, pollingInterval string

	//loggerSetup(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr)
	loggerSetup(os.Stdout, os.Stdout, os.Stdout, os.Stderr)

	flag.StringVar(&router, "router", "http://192.168.1.1/JNAP/", "router endpoint")
	flag.StringVar(&broker, "broker", "", "mqtt broker")
	flag.StringVar(&user, "user", "admin", "router user")
	flag.StringVar(&pass, "pass", "", "router password")
	flag.StringVar(&pollingInterval, "interval", "5m", "router polling interval")
	flag.Parse()

	interval, err := time.ParseDuration(pollingInterval)
	if err != nil {
		Error.Println("error parsing pollingInterval arg with value", pollingInterval)
		panic(err)
	}

	opts := MQTT.NewClientOptions().AddBroker(broker)
	opts.SetClientID("go-seared")

	//create and start a client using the above ClientOptions
	c := MQTT.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		Error.Println("Couldn't connect to MQTT server", broker)
		panic(token.Error())
	}
	Info.Println("Connected to", broker)

	j := jnap.NewJNAP(router, user, pass)

	devicesTicker := time.NewTicker(interval)
	debugTicker := time.NewTicker(time.Minute * 1)
	quit := make(chan struct{})

	// Main loop
	for {
		select {
		case <-devicesTicker.C:
			devices, revision, err := j.GetDevices()
			if err != nil {
				Error.Println("Couldn't fetch device list from router")
				return
			}
			Info.Println("Requested devices to router")
			Info.Println("Got revision", revision)

			token := c.Publish("network/router/devices/revision", 0, false, strconv.FormatInt(revision, 10))
			token.Wait()

			Trace.Println("Listing connected devices:")
			for i, device := range devices {
				var ip string
				publishDevice(c, &device)

				for _, conn := range device.Connections {
					ip = conn.IPAddress
				}

				if ip != "" {
					Trace.Println(i, device.FriendlyName, ip)
				}
			}
		case <-debugTicker.C:
			Trace.Println("Heartbeat ticking")
		case <-quit:
			devicesTicker.Stop()
			debugTicker.Stop()
			return
		}
	}
}

func loggerSetup(
	traceHandle io.Writer,
	infoHandle io.Writer,
	warningHandle io.Writer,
	errorHandle io.Writer) {

	Trace = log.New(traceHandle,
		"TRACE: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Info = log.New(infoHandle,
		"INFO: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Warning = log.New(warningHandle,
		"WARNING: ",
		log.Ldate|log.Ltime|log.Lshortfile)

	Error = log.New(errorHandle,
		"ERROR: ",
		log.Ldate|log.Ltime|log.Lshortfile)
}

func publishDevice(c MQTT.Client, dev *jnap.Device) {
	basePath := "network/router/devices/" + dev.DeviceID + "/"

	// <dev>/lastChangeRevision
	token := c.Publish(basePath+"lastChangeRevision", 0, false, strconv.FormatInt(dev.LastChangeRevision, 10))
	token.Wait()

	// <dev>/friendlyName
	token = c.Publish(basePath+"friendlyName", 0, false, dev.FriendlyName)
	token.Wait()

	// <dev>/model/deviceType
	if dev.Model.DeviceType != "" {
		token = c.Publish(basePath+"model/deviceType", 0, false, dev.Model.DeviceType)
		token.Wait()
	}

	// <dev>/model/manufacturer
	if dev.Model.Manufacturer != "" {
		token = c.Publish(basePath+"model/manufacturer", 0, false, dev.Model.Manufacturer)
		token.Wait()
	}

	// <dev>/model/modelNumber
	if dev.Model.ModelNumber != "" {
		token = c.Publish(basePath+"model/modelNumber", 0, false, dev.Model.ModelNumber)
		token.Wait()
	}

	// <dev>/unit/operatingSystem
	if dev.Unit.OperatingSystem != "" {
		token = c.Publish(basePath+"unit/operatingSystem", 0, false, dev.Unit.OperatingSystem)
		token.Wait()
	}

	for _, conn := range dev.Connections {
		publishDeviceConnection(c, basePath, &conn)
	}

	for _, prop := range dev.Properties {
		token = c.Publish(basePath+"properties", 0, false, prop.Name+":"+prop.Value)
		token.Wait()
	}
}

func publishDeviceConnection(c MQTT.Client, basePath string, conn *jnap.DeviceConnection) {
	var token MQTT.Token

	// <dev>/connections/macAddress
	if conn.MacAddress != "" {
		token = c.Publish(basePath+"connections/macAddress", 0, false, conn.MacAddress)
		token.Wait()
	}

	// <dev>/connections/ipAddress
	if conn.MacAddress != "" {
		token = c.Publish(basePath+"connections/ipAddress", 0, false, conn.IPAddress)
		token.Wait()
	}
}
